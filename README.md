# GEESE

This repository contains the compaining code for GEESE: Metabolicaly driven latent space learning for 
gene expression data.


Publication
===========



Requirements
============

- [DEAP](https://github.com/DEAP/deap)
- [COBRApy](https://opencobra.github.io/cobrapy/)
- [Google Optimization Tools (OR-Tools)](https://developers.google.com/optimization/), Python library
- [Keras](https://keras.io/), with [TensorFlow](https://github.com/tensorflow/tensorflow) backend
- [Seaborn](https://seaborn.pydata.org/) for some fancy plotting


Files and folders
=================
- `generate_pareto.py` is used to generate a particular pareto front
- `merge_data-fluxes.py` produces a dataset of Gene Expression and fluxes, given a folder with different 
Paretos generated by `generate_pareto.py`
- `GEESE_train.ipynb` is a jupyter notebook used in training the GEESE(VAE+FBA) model.
- `GEESE.ipynb` is a jupyter notebook used for applying the GEESE to the unseen real dataset
- `Sensitivity.ipynb` is a jupyter notebook used for performing sensitivity
  analysis.
- `Gene_importance_analysis.ipynb` is a jupyter notebook used in evaluating
  genes along a pareto front.

- `realDataset` contains the relevant files for the real dataset. 
How to run
==========
1. Run `generate_pareto.py` with different oxygen/glucose concentration, in order to generate a dataset
2. Run `merge_data-fluxes.py` to merge the different paretos
3. Run `real_data-fluxes.py` to preprocess the real dataset
4. Use `GEESE_train.ipynb` to train the model (Or use the weigths provided).
5. Use `GEEESE.ipynb` to explore the unseen real dataset.

6. Use `Sensitivity.ipynb` and `Gene_importance_analysis.ipynb` to analyse gene
   along a particular pareto.
Apply GEESE to new data
=======================
0. Find the metabolic model for the organism. Extract oxigen/glucose concentrations.

Run steps 1 to 4 from **How to run**.

Run step 5 with your dataset. 
