#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 10:28:28 2018

@author: marcobarsacchi
"""

import copy
import fba
from gene_utils import apply_r_set


def evaluate(individual,genes,model,p_model,rules,fluxes=False):
    local_model = copy.copy(model)
    expression_dict = {gene:value for gene,value in zip(genes, individual)}
#    local_model = apply_reaction_set(local_model, expression_dict, p_model)
    local_model = apply_r_set(local_model, expression_dict, p_model, genes, rules)

    return fba.solve_or_bylayer_norm(local_model, solver = 'ortools',fluxes=fluxes)
    
