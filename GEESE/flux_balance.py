#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import copy

from cobra import Metabolite, Reaction, Model
"""
Created on Mon Nov 20 12:23:17 2017

@author: marcobarsacchi
"""
"""
[xopt, fmin, status, extra] = glpk (c, a, b, lb, ub, ctype, vartype,
sense, param)

Solve an LP/MILP problem using the GNU GLPK library. Given three
arguments, glpk solves the following standard LP:

min C'*x subject to A*x  <= b

but may also solve problems of the form

[ min | max ] C'*x
subject to
  A*x [ "=" | "<=" | ">=" ] b
  x >= LB
  x <= UB

Input arguments:
c = A column array containing the objective function coefficients.

A = A matrix containing the constraints coefficients.

b = A column array containing the right-hand side value for each constraint
    in the constraint matrix.

lb = An array containing the lower bound on each of the variables.  If
     lb is not supplied (or an empty array) the default lower bound for the variables is
     minus infinite.

ub = An array containing the upper bound on each of the variables.  If
     ub is not supplied (or an empty array) the default upper bound is assumed to be
     infinite.

ctype = An array of characters containing the sense of each constraint in the
        constraint matrix.  Each element of the array may be one of the
        following values
          'F' Free (unbounded) variable (the constraint is ignored).
          'U' Variable with upper bound ( A(i,:)*x <= b(i)).
          'S' Fixed Variable (A(i,:)*x = b(i)).
          'L' Variable with lower bound (A(i,:)*x >= b(i)).
          'D' Double-bounded variable (A(i,:)*x >= -b(i) and A(i,:)*x <= b(i)).
 
vartype = A column array containing the types of the variables.
              'C' Continuous variable.
              'I' Integer variable
              'B' Binary variable
"""


def flux_balance(model,second_obj='EX_ac_LPAREN_e_RPAREN_'):
    """FLUX_BALANCE Flux-balance analysis of FBA model
        [V, FMAX, FMIN] = FLUX_BALANCE(FBAMODEL) performs a basic flux-balance
        analysis of the FBA model FBAMODEL and returns a biomass-maximizing
        flux distribution in the vector V.  The maximimum and minimum 
        synthetic objective possible in a biomass-maximizing flux distribution
        is given in FMAX and FMIN, respectively.
    """
    fbamodel = model
    biomass_solution = fbamodel.optimize()
    biomass = biomass_solution.objective_value
    fbamodel.reactions.Ec_biomass_iJO1366_core_53p95M.bounds = (biomass-0.01,biomass+0.01)
    fbamodel.objective = second_obj
    solution_ac_u = fbamodel.optimize()
    #fbamodel.objective.direction='min'
    #solution_ac_l = fbamodel.optimize()

    return (biomass_solution.objective_value,solution_ac_u.objective_value)
    
    
    
    
    