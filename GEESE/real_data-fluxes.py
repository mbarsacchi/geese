import random 
import os 
import copy as copy

import numpy as np
import scipy.io as sio

from fitness import evaluate
import fba

base_folder='realDataset/'
seed = 0

#READING SOME DATA
gene_values = sio.matlab.loadmat(base_folder+'data_only.mat')['data_only']
genes = [gene[0][0] for gene in sio.loadmat(base_folder+'geni.mat')['geni']]
probe_genes = [gene[0][0] for gene in sio.loadmat(base_folder+'probe_genes.mat')['probe_genes']]
def safe_list_get(l, idx, default):
    try:
        return l[idx]
    except IndexError:
        return default
oxygen = [gene[0][0] for gene in sio.loadmat(base_folder+'oxygen.mat')['oxygen']]
glucose = [safe_list_get(gene[0],0,'') for gene in sio.loadmat(base_folder+'glucose.mat')['glucose']]
name_conditions = [gene[0][0] for gene in sio.loadmat(base_folder+'name_conditions.mat')['name_conditions']]
name_conditions_with_replicates = [gene[0][0] for gene in sio.loadmat(base_folder+'name_conditions_with_replicates.mat')['name_conditions_with_replicates']]


model = fba.load_mat_atp('iJO1366_Ecoli_atp.mat')
model.g = np.ones_like(model.g)
p_model = fba.load_mat_atp('iJO1366_Ecoli_atp.mat')
p_model.g = np.ones_like(p_model.g)

rules = [str(rule[0]).replace("'",'').replace('[','').replace(']','') for rule in sio.loadmat(base_folder + 'geni_reazioni.mat')['geni_reazioni']]

gene_index = []
for gene in genes:
    try:
        match = probe_genes.index(gene)
        gene_index.append(match)
    except ValueError:
        gene_index.append(None)

gene_filtered = np.zeros((len(genes),gene_values.shape[1]))
m,n = gene_filtered.shape
for j in range(m):
    for i in range(n):
        if gene_index[j] is None:
            gene_filtered[j][i] =  1.
        else:
            gene_filtered[j][i] = gene_values[gene_index[j]][i]
            


num_examples = gene_filtered.shape[1]
np.random.seed(seed)
GE = np.zeros((gene_filtered.shape[0]+2, num_examples))
fluxes = np.zeros((model.nrnx, num_examples))
num_var = gene_filtered.shape[1]

for k in range(num_examples):
    found = [ind for ind,cond in enumerate(name_conditions_with_replicates) if cond.startswith(name_conditions[k])]
    found = found[0]
    if oxygen[found] == 'aerobic':
        model.lb[model.reactions.index('EX_o2(e)')] = -10
        p_model.lb[p_model.reactions.index('EX_o2(e)')] = -10
        GE[-2,k] = -10
    else:
        model.lb[model.reactions.index('EX_o2(e)')] = 0.
        p_model.lb[p_model.reactions.index('EX_o2(e)')] = 0.
        GE[-2,k] = 0

    if glucose[found] == '':
        model.lb[model.reactions.index('EX_glc(e)')] = -10
        p_model.lb[p_model.reactions.index('EX_glc(e)')] = -10
        GE[-1,k] = -10
    else:
        model.lb[model.reactions.index('EX_glc(e)')] = -float(glucose[found])
        p_model.lb[p_model.reactions.index('EX_glc(e)')] = -float(glucose[found]) 
        GE[-1,k] = -float(glucose[found]) 
        
    GE[:-2,k] = gene_filtered[:,k]
    a,b,fluxes[:,k] = evaluate(GE[2:,k],genes,model,p_model,rules,True)
    if k%200 == 0:
        print('Iteration: %d of %d' %(k+1,num_examples))

np.save('realDataset/GE_real.npy', GE)
np.save('realDataset/fluxes_real.npy', fluxes)