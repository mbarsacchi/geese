#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 10:50:24 2017

@author: marcobarsacchi
"""

import random 
import multiprocessing
import time
from fitness import evaluate

import numpy as np
from deap import tools, base, creator
import scipy
from functions import mutPolynomialGenes 

use_multiprocessing = True
# hypervolume related stuff
try:
    # try importing the C version
    from deap.tools._hypervolume import hv as hv
except ImportError:
    # fallback on python version
    from deap.tools._hypervolume import pyhv as hv

import fba 


def hypervolume(front, **kargs):
    """Evaluate hypervolume indicator w.r.t a reference point.
    The provided *front* should be a set of non-dominated
    individuals having each a :attr:`fitness` attribute. 
    """
    
    # Must use wvalues * -1 since hypervolume use implicit minimization
    # And minimization in deap use max on -obj
    wobj = np.array([ind.fitness.wvalues for ind in front]) * -1
    ref = kargs.get("ref", None)
    if ref is None:
        ref = np.max(wobj, axis=0) + 1
    return hv.hypervolume(wobj, ref)
        


model = fba.load_mat_atp('iJO1366_Ecoli_atp.mat')
#model.present[330] = 0
model.g = np.ones_like(model.g)
#model.S[330,330] = 0.
#model.S[434,330] = -1.
#model.ub[330]  =1000
#model.reaction_names[330] = 'NADH production'
#model.reactions[330] = 'EX_NADH(e)'

p_model = fba.load_mat_atp('iJO1366_Ecoli_atp.mat')
#p_model.present[330] = 0
p_model.g = np.ones_like(model.g)
#p_model.S[330,330] = 0.
#p_model.S[434,330] = -1.
#p_model.ub[330]  = 1000
#p_model.reaction_names[330] = 'NADH production'
#p_model.reactions[330] = 'EX_NADH(e)'



"""
('', 'aerobic'),
 ('', 'anaerobic'),
 ('11', 'aerobic'),
 ('11', 'anaerobic'),
 ('20', 'aerobic'),
 ('20', 'anaerobic'),
 ('22', 'aerobic'),
 ('22', 'anaerobic'),
 ('44', 'aerobic'),
 ('5.5', 'aerobic'),
 ('5.6', 'aerobic'),
 ('5.6', 'anaerobic'),
 ('6 ', 'aerobic')}
"""
is_aerobic = True
glucose = ''

if is_aerobic:
    model.lb[model.reactions.index('EX_o2(e)')] = -10
    p_model.lb[p_model.reactions.index('EX_o2(e)')] = -10
    aerb = -10
else:
    model.lb[model.reactions.index('EX_o2(e)')] = 0.
    p_model.lb[p_model.reactions.index('EX_o2(e)')] = 0.
    aerb = 0
if glucose == '':
    model.lb[model.reactions.index('EX_glc(e)')] = -10
    p_model.lb[p_model.reactions.index('EX_glc(e)')] = -10
    gluc =-10
else:
    model.lb[model.reactions.index('EX_glc(e)')] = -float(glucose)
    p_model.lb[p_model.reactions.index('EX_glc(e)')] = -float(glucose) 
    gluc = -float(glucose)

rules = [str(rule[0]).replace("'",'').replace('[','').replace(']','') for rule in scipy.io.loadmat('geni_reazioni.mat')['geni_reazioni']]
genes = [gene[0][0] for gene in scipy.io.loadmat('geni.mat')['geni']]
IND_SIZE = 1367


###

min_v = [0.] * len(genes)
max_v = [100.] * len(genes)
def init_attribute():
    return 1. + 2.*(np.random.uniform()-0.5) 
 

creator.create("FitnessMax", base.Fitness, weights=(1.0,-1.0))
creator.create("Individual", list, fitness=creator.FitnessMax)
       
toolbox = base.Toolbox()

toolbox.register("attribute", init_attribute)
toolbox.register("individual", tools.initRepeat, creator.Individual,
                 toolbox.attribute, n=IND_SIZE)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("mate", tools.cxSimulatedBinaryBounded, eta=20, low=min_v, up=max_v)
#toolbox.register("mutate", tools.mutPolynomialBounded, eta = 20, indpb=0.5, low=min_v, up=max_v)
toolbox.register("mutate", mutPolynomialGenes, eta= 20, indpb=0.5, low=min_v, up=max_v)
toolbox.register("select", tools.selRandom)
toolbox.register("evaluate", evaluate, genes =genes, model=model, p_model =p_model, rules=rules)

if use_multiprocessing:
    num_cores = 2
    try:
        pool = multiprocessing.Pool(multiprocessing.cpu_count()-2)
    except:
        pool = multiprocessing.Pool(num_cores)
    toolbox.register("map", pool.map)

def main(NGEN,pop_size):
    

    hypervolumes = []
    
    stats_fit = tools.Statistics(key=lambda ind: ind.fitness.values)
    stats_size = tools.Statistics(key=len)
    mstats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
    mstats.register("avg", np.mean, axis=0)
    mstats.register("std", np.std, axis=0)
    mstats.register("min", np.min, axis=0)
    mstats.register("max", np.max, axis=0)
    logbook = tools.Logbook()
    logbook.header = "gen", "fitness", "hypervolume", "time", "pareto_size"
    logbook.chapters["fitness"].header = "min", "max"

    pop = toolbox.population(n=pop_size)
    CXPB, MUTPB = 0.9, 0.1
    pareto = tools.ParetoFront()
    # Evaluate the entire population
    fitnesses = toolbox.map(toolbox.evaluate, pop)
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit
        
    pop[:] = tools.selNSGA2(pop,pop_size)
    for g in range(NGEN):
        tstart = time.time()
        # Select the next generation individuals, currently doing it 
        # using a random sample
        offspring = toolbox.select(pop, len(pop))
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))

        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < CXPB:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values
            else:
                toolbox.mutate(child1)
                toolbox.mutate(child2)
                del child1.fitness.values
                del child2.fitness.values
                
#        for mutant in offspring:
#            if random.random() < MUTPB:
#                toolbox.mutate(mutant)
#                del mutant.fitness.values

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # The population is NSGA2 selected from the older population + the offspring
        pop[:] = tools.selNSGA2(pop+offspring,pop_size)
        record = mstats.compile(pop)
        pareto.update(pop)
        
        volume = hypervolume(pareto)#, ref = np.zeros(2))
        hypervolumes.append(volume)
        eval_time = time.time() - tstart
        logbook.record(gen=g, hypervolume=volume,
                       time=eval_time, pareto_size=len(pareto), **record)
        print(logbook.stream)
        
    return pop, pareto, hypervolumes


if __name__ == "__main__":
    
    import pickle
    pop,pareto, hyp = main(30,20)  
    # Specify glucose and aerobic
    is_aerobic = True
    glucose = ''

    import matplotlib.pyplot as plt 
    points = list(zip(*[p.fitness.values for p in pareto]))
    plt.scatter(points[1],points[0],marker='*',color='r')
    plt.plot(points[1],points[0],'r--',linewidth=0.5)
    plt.ylabel('$f_1$')
    plt.xlabel('$f_2$')
    plt.title('Pareto')
    plt.figure()
    plt.plot(range(1,len(hyp)+1), hyp)
    plt.ylabel('Hypervolume')
    plt.xlabel('Iteration #')
    nparray = np.array([itemoz +[aerb,gluc] for itemoz in pareto.items])
    np.save('pareto_%d_%f.npy'%(aerb,gluc),nparray)