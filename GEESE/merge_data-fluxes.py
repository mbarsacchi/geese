import random 
import multiprocessing
import time

import os 
import numpy as np
import copy as copy
import scipy

from fitness import evaluate
import fba

base_folder=''
seed = 0
dataPath = '/Users/marcobarsacchi/Development/DATA/'

model = fba.load_mat_atp(base_folder + 'iJO1366_Ecoli_atp.mat')
model.g = np.ones_like(model.g)
p_model = fba.load_mat_atp(base_folder + 'iJO1366_Ecoli_atp.mat')
p_model.g = np.ones_like(p_model.g)

rules = [str(rule[0]).replace("'",'').replace('[','').replace(']','') for rule in scipy.io.loadmat(base_folder + 'geni_reazioni.mat')['geni_reazioni']]


file_list = filter(lambda name: name.startswith('pareto_'),os.listdir(dataPath))
print('Found data files' )
loaded_matrix = np.vstack([np.load(dataPath+fname) for fname in file_list]).T

rules = [str(rule[0]).replace("'",'').replace('[','').replace(']','') for rule in scipy.io.loadmat('geni_reazioni.mat')['geni_reazioni']]
genes = [gene[0][0] for gene in scipy.io.loadmat('geni.mat')['geni']]
IND_SIZE = 1367

num_examples = loaded_matrix.shape[1]
np.random.seed(seed)
fluxes = np.zeros((model.nrnx, num_examples))

for k in range(num_examples):

    model.lb[model.reactions.index('EX_o2(e)')] = loaded_matrix[-2,k]
    p_model.lb[p_model.reactions.index('EX_o2(e)')] = loaded_matrix[-2,k]


    model.lb[model.reactions.index('EX_glc(e)')] = loaded_matrix[-1,k]
    p_model.lb[p_model.reactions.index('EX_glc(e)')] = loaded_matrix[-1,k]


    a,b,fluxes[:,k] = evaluate(loaded_matrix[:-2,k],genes,model,p_model,rules,True)

    if k%500 == 0:
        print('Iteration: %d of %d' %(k+1, num_examples))

np.save('generated_dataset/GE.npy',loaded_matrix)
np.save('generated_dataset/fluxes.npy',fluxes)