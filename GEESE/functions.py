#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 16:32:19 2018

@author: marcobarsacchi
"""
from itertools import repeat
from collections import Sequence
import random

def mutUniformFloat(individual, low, up, indpb):
    """Mutate an individual by replacing attributes, with probability *indpb*,
    by a float uniformly drawn between *low* and *up* inclusively.
    
    :param individual: :term:`Sequence <sequence>` individual to be mutated.
    :param low: The lower bound or a :term:`python:sequence` of
                of lower bounds of the range from wich to draw the new
                integer.
    :param up: The upper bound or a :term:`python:sequence` of
               of upper bounds of the range from wich to draw the new
               integer.
    :param indpb: Independent probability for each attribute to be mutated.
    :returns: A tuple of one individual.
    """
    size = len(individual)
    if not isinstance(low, Sequence):
        low = repeat(low, size)
    elif len(low) < size:
        raise IndexError("low must be at least the size of individual: %d < %d" % (len(low), size))
    if not isinstance(up, Sequence):
        up = repeat(up, size)
    elif len(up) < size:
        raise IndexError("up must be at least the size of individual: %d < %d" % (len(up), size))
    
    for i, xl, xu in zip(range(size), low, up):
        if random.random() < indpb:
            individual[i] = random.random()*(xu-xl) + xl
    
    return individual,

def mutPolynomialGenes(individual, eta, low, up, indpb):
    """Modified polynomial mutation from the one implemented in 
    the original NSGA-II algorithm in C by Deb.
    
    :param individual: :term:`Sequence <sequence>` individual to be mutated.
    :param eta: Crowding degree of the mutation. A high eta will produce
                a mutant resembling its parent, while a small eta will
                produce a solution much more different.
    :param low: A value or a :term:`python:sequence` of values that
                is the lower bound of the search space.
    :param up: A value or a :term:`python:sequence` of values that
               is the upper bound of the search space.
    :returns: A tuple of one individual.
    """
    size = len(individual)
    if not isinstance(low, Sequence):
        low = repeat(low, size)
    elif len(low) < size:
        raise IndexError("low must be at least the size of individual: %d < %d" % (len(low), size))
    if not isinstance(up, Sequence):
        up = repeat(up, size)
    elif len(up) < size:
        raise IndexError("up must be at least the size of individual: %d < %d" % (len(up), size))
    
    for i, xl, xu in zip(range(size), low, up):
        x = individual[i]

        rand = random.random()
        mut_pow = 1.0 / (eta + 1.)

        if rand < 0.5:
            val = 2.0 * rand 
            delta_q = val**mut_pow - 1.0
        else:
            val = 2.0 * (1.0 - rand) 
            delta_q = 1.0 - val**mut_pow

        x = x + delta_q
        x = min(max(x, xl), xu)
        individual[i] = x
    return individual,
