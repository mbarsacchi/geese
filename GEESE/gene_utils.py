#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 16:06:09 2017

@author: marcobarsacchi
"""
import numpy as np

# Function for relating gene expression to reaction bounds
def h(y):
    if y == 1:
        return 1.
    elif y== 0:
        return 0.
    else:
        return (1 + np.abs(np.log(y)))**np.sign(y-1)

# Gene class, (hack for evaluating expression)
class Gene(float):
    def __add__(self, other):
        return Gene(max(self, other))      
    def __mul__(self, other):
        return Gene(min(self, other))

    
# Apply gene expression to data
def apply_reaction_set(model, expression_dict,p_model):

    for reaction in model.reactions:
        value = eval_reaction(reaction, expression_dict)
        
        if value != np.inf:
            cbounds = p_model.reactions.get_by_id(reaction.id).bounds
            lb = h(value)*cbounds[0]
            ub = h(value)*cbounds[1]
            model.reactions.get_by_id(reaction.id).bounds = (lb,ub)
    return model

def apply_r_set(model, expression_dict,p_model, genes, rules):
    for ind,current_rule in enumerate(rules):
        
        # Empty set
        implied_genes = frozenset(gene for gene in current_rule.replace('(','').replace(')','').replace('or','').replace('and','').split(' ') if gene is not '')
        if implied_genes != frozenset():
            for gene in implied_genes:
                exec('%s=Gene(%f)'%(gene,expression_dict[gene]))
            rule = current_rule
            # Rules are written as and/or relationships. We replace or with + and
            # and with * . The Gene class performs addition a+b as max(a,b) and
            # product a * b as min(a,b)
            rule = rule.replace('or','+').replace('and','*')
            value = float(eval(rule))

            if value != np.inf:
                cbounds = (p_model.lb[ind],p_model.ub[ind])
                model.lb[ind] = h(value)*cbounds[0]
                model.ub[ind] = h(value)*cbounds[1]
            if ind<20:
                print(value)
    return model

def apply_r_set_v2(model, expression_dict,p_model, genes, rules):
    for ind,current_rule in enumerate(rules):
        
        # Empty set
        implied_genes = frozenset(gene for gene in current_rule.replace('(','').replace(')','').replace('+','').replace('min','').replace(',','').replace(' ','').replace("'",'').split('/') if gene is not '')
        if implied_genes != frozenset():
            for gene in implied_genes:
                exec('%s=%f'%(gene,expression_dict[gene]))
            rule = current_rule
            # Rules are written as and/or relationships. We replace or with + and
            # and with * . The Gene class performs addition a+b as max(a,b) and
            # product a * b as min(a,b)
            rule = rule.replace('/','').replace("'",'')
            value = float(eval(rule))
            if value != np.inf:
                cbounds = (p_model.lb[ind],p_model.ub[ind])
                model.lb[ind] = h(value)*cbounds[0]
                model.ub[ind] = h(value)*cbounds[1]
    return model


# Single reaction action
def eval_reaction(reaction, expression_dict):
    
    implied_genes = frozenset(gene.id for gene in reaction.genes)
    # Empty set
    if implied_genes == frozenset():
        return np.inf
    for gene in implied_genes:
        exec('%s=Gene(%f)'%(gene,expression_dict[gene]))
    rule = reaction.gene_reaction_rule
    rule = rule.replace('or','+').replace('and','*')
    return float(eval(rule))
    
def get_genes(model):
    gene_set = frozenset(gene.id for gene in model.genes)
    return gene_set

    