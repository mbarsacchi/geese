import cobra
import numpy as np
import scipy.sparse
import logging

logger = logging.getLogger("fba")

"""
[xopt, fmin, status, extra] = glpk (c, a, b, lb, ub, ctype, vartype,
sense, param)

Solve an LP/MILP problem using the GNU GLPK library. Given three
arguments, glpk solves the following standard LP:

min C'*x subject to A*x  <= b

but may also solve problems of the form

[ min | max ] C'*x
subject to
  A*x [ "=" | "<=" | ">=" ] b
  x >= LB
  x <= UB

Input arguments:
c = A column array containing the objective function coefficients.

A = A matrix containing the constraints coefficients.

b = A column array containing the right-hand side value for each constraint
    in the constraint matrix.

lb = An array containing the lower bound on each of the variables.  If
     lb is not supplied (or an empty array) the default lower bound for the variables is
     minus infinite.

ub = An array containing the upper bound on each of the variables.  If
     ub is not supplied (or an empty array) the default upper bound is assumed to be
     infinite.

ctype = An array of characters containing the sense of each constraint in the
        constraint matrix.  Each element of the array may be one of the
        following values
          'F' Free (unbounded) variable (the constraint is ignored).
          'U' Variable with upper bound ( A(i,:)*x <= b(i)).
          'S' Fixed Variable (A(i,:)*x = b(i)).
          'L' Variable with lower bound (A(i,:)*x >= b(i)).
          'D' Double-bounded variable (A(i,:)*x >= -b(i) and A(i,:)*x <= b(i)).
 
vartype = A column array containing the types of the variables.
              'C' Continuous variable.
              'I' Integer variable
              'B' Binary variable
"""

def fba_solver(model, obj = 'EX_ac_LPAREN_e_RPAREN_'):
    nrxn = len(model.reactions)
    nmetab = len(model.metabolites)
    yt = np.array([float(reaction.functional) for reaction in model.reactions])
    S = model.S
    ynt = np.logical_not(yt)
    L = scipy.sparse.csr_matrix(((np.ones((np.count_nonzero(ynt))),(np.arange(1,np.count_nonzero(ynt)),
                           np.nonzero(ynt)[0]))
                           ),(np.count_nonzero(ynt),
                           nrxn)
                           )
    
    A = scipy.sparse.vstack((S,L))
    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt))))
    
    ctype = np.array(['S']*len(b))
    vartype = np.array(['C']*nrxn)
    
    # Solve first time
    # [v, vbiomass] = glpk(fbamodel.f, A, b, fbamodel.vmin, fbamodel.vmax, ctype, vartype, -1, param);
    v = solve(np.array(model.objective_coefficients),
                                    A, b,np.array(model.lower_bounds), 
                                    np.array(model.upper_bounds))
    vbiomass = v[np.nonzero(model.objective_coefficients)]
    
    A = scipy.sparse.vstack((S,L,np.array(model._objective_coefficients)))
    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt)),np.array(vbiomass).reshape(1)))
    ctype = np.array(['S']*(len(b)+1))
    
    # [v, vbiomass] = glpk(fbamodel.f, A, b, fbamodel.vmin, fbamodel.vmax, ctype, vartype, -1, param);
    
    coefficients = np.zeros_like(np.array(model._objective_coefficients))
    coefficients[model.reactions.index(obj)] = 1.0
    v_2 = solve(coefficients, A,b ,np.array(model.lower_bounds), 
                                    np.array(model.upper_bounds))
    fmax = v_2[np.nonzero(coefficients)]
    return (vbiomass[0], fmax[0])
    



# Mosek solver(faster)
#try: from mosek.fusion import Model, Domain, Expr, ObjectiveSense
#except ImportError: raise ValueError("invalid option "\
#            "(solver = 'mosek'): mosek is not installed")

#def solve_mosek(c, A, b, lb, ub):

#    with Model("linear") as M:
#        x = M.variable("x",A.shape[1])
#        M.setSolverParam("optimizer", "intpnt")
#        M.setSolverParam("intpntSolveForm","primal")
        # constraints
#        for k in range(A.shape[0]):
#            M.constraint("c%d"%k, Expr.dot(A[k], x), Domain.equalsTo(b[k]))
#        for j in range(lb.shape[0]):
#            M.constraint("x_low%d"%j, x.index(j), Domain.greaterThan(lb[j]))
#            M.constraint("x_up%d"%j, x.index(j), Domain.lessThan(ub[j]))
#        M.objective("obj", ObjectiveSense.Maximize, Expr.dot(c, x))
        
        # Solve the problem
#        tp = time.time()

#        M.solve()
#        print(time.time()-tp)
        # Get the solution values
#        sol = x.level()
#        return sol

#try: from cvxopt import matrix, glpk
#except ImportError: raise ValueError("invalid option "\
#        "(solver = 'glpk'): cvxopt.glpk is not installed")
#def solve_cvxopt(c, A, b, lb, ub, solver='glpk'):

#    glpk.options['msg_lev'] = 'GLP_MSG_OFF'
#    c_opt = matrix(-c)
#    size = len(c)
#    G = matrix(np.vstack((np.eye(size),-np.eye(size))))
#    h = matrix(np.concatenate((ub,-lb)))
#    status,x,s,z = glpk.lp(c_opt, G, h, matrix(A), matrix(b))
#    try:
#        return np.array(x)[:,0]
#    except:
#        logger.warning('Error in solving fba: '+status)
#        return np.zeros(size)
        
#try: from pulp import LpProblem, LpMaximize, LpVariable, LpStatus, solvers, LpAffineExpression, LpConstraint, LpConstraintEQ
#except ImportError: raise ValueError("invalid option "\
#        "(solver = 'pulp'): pulp is not installed")
#
#def solve_pulp(c, A, b, lb, ub):
#    print('Start Preparing')
#
#    prob = LpProblem("fba", LpMaximize)
#    xs = [LpVariable("x%d" %i, lowBound=lbo, upBound=ubo) for i,lbo,ubo in zip(range(len(c)), lb, ub)]
#    print(A.shape[0])
#    for k in range(A.shape[0]):
#        constr = LpAffineExpression((x,w) for x, w in zip(xs, A[k,:]) if w!=0)
#        prob += constr == b[k], "Constraint_%d" %k
#        #prob += LpConstraint(constr,LpConstraintEQ, "Constraint_%d" %k, b[k])
#    prob += LpAffineExpression((x,s) for x,s in zip(xs,c) if s!=0)
#    if A.shape[0] == 1805:
#        prob.writeLP('test.lp')
#    prob.solve()
#    #prob.solve()#solvers.COIN_CMD(msg=1))
#    print("Status:", LpStatus[prob.status])
#    variab = prob.variablesDict()
#    return np.array([variab['x%d'%k].varValue for k in range(len(c))])
    
# Scipy solver   
from scipy.optimize import linprog
def solve_linprog(c, A, b, lb, ub):
    
    res = linprog(-c, A_eq=A, b_eq=b, bounds=list(zip(lb,ub)),method='interior-point', options={'disp':True,'sym_pos':False,'lstsq':True})
    return res.x
    
    
try:
    from optlang import Model, Variable, Constraint, Objective
    from optlang.symbolics import Zero
except ImportError: raise ValueError("invalid option "\
        "(solver = 'opt'): optlang is not installed")

def solve_opt(c, A, b, lb, ub):
    variables = [Variable(name="x%d" %i, lb=lbo, ub=ubo) for i,lbo,ubo in zip(range(len(c)), lb, ub)]
   
    model = Model(name='fba')
    objective = Objective(sum(x*w for x, w in zip(variables, c) if w!=0))
    
    model.add(variables)
    model.objective = objective
    
    for k in range(A.shape[0]):
        constr = Constraint(Zero, ub=b[k] , lb=b[k])
        model.add(constr)
        model.constraints[-1].set_linear_coefficients({x:w for x, w in zip(variables, A[k,:]) if w!=0})
    
    status = model.optimize()

    return np.array([var.primal for var_name, var in model.variables.items()])

    
try: from ortools.linear_solver import linear_solver_pb2,pywraplp
except ImportError: raise ValueError("invalid option "\
        "(solver = 'ortools'): ortools is not installed")
    
def solve_or(c, A, b, lb, ub):
    solver = pywraplp.Solver('fba',
                           pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
    variables = [solver.NumVar(low,up,'x_%d'%ind) for ind,low,up in zip(range(len(c)),lb,ub)]
    ind_c = np.nonzero(c)[0][0]
    constraints = []
    for k in range(A.shape[0]):
        constr = solver.Constraint(b[k],b[k])
        r = A[k,:]
        for index in r.nonzero()[0]:
            constr.SetCoefficient(variables[index],r[index])
        constraints.append(constr)
    objective = solver.Objective()
    objective.SetCoefficient(variables[ind_c],1)
    objective.SetMaximization()

    solver.Solve()
    return np.array([variables[ind].solution_value() for ind in range(len(c))])


def solve(c, A, b, lb, ub, solver ='cvxopt'):
    if solver=='scipy':
        return solve_linprog(c, A, b, lb, ub)
    elif solver=='mosek':
        return solve_mosek(c, A, b, lb, ub)
    elif solver =='cvxopt':
        return solve_cvxopt(c, A, b, lb, ub)
    elif solver == 'pulp':
        return solve_pulp(c, A, b, lb, ub)
    elif solver == 'opt':
        return solve_opt(c, A, b, lb, ub)
    elif solver == 'ortools':
        return solve_or(c, A, b, lb, ub)
    else:
        raise Exception('Unknown solver: %s' %solver)
            


#def solve_fba_model(model, solver ='cvxopt'):
#    nrxn = model.nrnx
#    nmetab = model.nmetabs
#    yt = model.present
#    S = model.S
#    ynt = np.logical_not(yt)
#    L = scipy.sparse.csr_matrix(((np.ones((np.count_nonzero(ynt))),(np.arange(1,np.count_nonzero(ynt)),
#                           np.nonzero(ynt)[0]))
#                           ),(np.count_nonzero(ynt),
#                           nrxn)
#                           )
#    
#    A = scipy.sparse.vstack((S,L))
#    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt))))
#    
#    
#    # Solve first time
#    v = solve(model.f, A.toarray(), b, model.lb, 
#                                    model.ub, solver=solver)
#    vbiomass = v[np.nonzero(model.f)]
#    
#    A = scipy.sparse.vstack((S,L,np.array(model.f)))
#    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt)),np.array(vbiomass).reshape(1)))
#    
#    
#    v_2 = solve(model.g, A.toarray(),b ,model.lb, 
#                                    model.ub, solver=solver)
#    fmax = v_2[np.nonzero(model.g)]
#    return (vbiomass[0], fmax[0])

def solve_fba_model(model, solver ='cvxopt'):
    nrxn = model.nrnx
    nmetab = model.nmetabs
    yt = model.present
    S = model.S
    ynt = np.logical_not(yt)
    L = scipy.sparse.csr_matrix(((np.ones((np.count_nonzero(ynt))),(np.arange(1,np.count_nonzero(ynt)),
                           np.nonzero(ynt)[0]))
                           ),(np.count_nonzero(ynt),
                           nrxn)
                           )
    
    A = scipy.sparse.vstack((S,L))
    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt))))
    
    
    # Solve first time
    v = solve(model.f, A.toarray(), b, model.lb, 
                                    model.ub, solver=solver)
    vbiomass = v[np.nonzero(model.f)]
    
    A = scipy.sparse.vstack((S,L,np.array(model.f)))
    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt)),np.array(vbiomass).reshape(1)))
    
    
    v_2 = solve(model.g, A.toarray(),b ,model.lb, 
                                    model.ub, solver=solver)
    fmax = v_2[np.nonzero(model.g)]
    return (vbiomass[0], v_2)

def solve_or_bylayer(model, solver ='glop',ptype = ['max','max']):
    nrxn = model.nrnx
    nmetab = model.nmetabs
    yt = model.present
    S = model.S
    ynt = np.logical_not(yt)
    L = scipy.sparse.csc_matrix(((np.ones((np.count_nonzero(ynt))),(np.arange(1,np.count_nonzero(ynt)),
                           np.nonzero(ynt)[0]))
                           ),(np.count_nonzero(ynt),
                           nrxn)
                           )
    
    A = scipy.sparse.vstack((S,L)).toarray()
    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt))))
    
    solver = pywraplp.Solver('fba',
                   pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
    variables = [solver.NumVar(low,up,'x_%d'%ind) for ind,low,up in zip(range(nrxn),model.lb,model.ub)]
    ind_c = np.nonzero(model.f)[0][0]
    constraints = []
    for k in range(A.shape[0]):
        constr = solver.Constraint(b[k],b[k])
        r = A[k,:]
        for index in r.nonzero()[0]:
            constr.SetCoefficient(variables[index],A[k,index])
    constraints.append(constr)
    objective = solver.Objective()
    objective.SetCoefficient(variables[ind_c],1)
    if ptype[0] == 'max':
        objective.SetMaximization()
    else:
        objective.SetMinimization()
    
    # Solve first time
    solver.Solve()
    
    vbiomass = variables[ind_c].solution_value()
    constr_bio = solver.Constraint(vbiomass,vbiomass)
    constr_bio.SetCoefficient(variables[ind_c],1)
    constraints.append(constr)
    ind_f = np.nonzero(model.g)[0]
    objective = solver.Objective()
    for ind in ind_f:
        objective.SetCoefficient(variables[ind],model.g[ind])
        
    if ptype[1] == 'max':
        objective.SetMaximization()
    else:
        objective.SetMinimization()
    #Solve second time
    solver.Solve()
    fmax = 0
    for ind in ind_f:
        fmax += variables[ind].solution_value() * model.g[ind]
    return (vbiomass, fmax)

def solve_or_bylayer_norm(model, solver ='glpk',ptype = ['max','min'],fluxes=False):
    """ Solve a bylayer problem, maximizing the Biomass production and
    minimizing the total flux.

    """
    nrxn = model.nrnx
    nmetab = model.nmetabs
    yt = model.present
    S = model.S
    ynt = np.logical_not(yt)
    L = scipy.sparse.csc_matrix(((np.ones((np.count_nonzero(ynt))),(np.arange(1,np.count_nonzero(ynt)),
                           np.nonzero(ynt)[0]))
                           ),(np.count_nonzero(ynt),
                           nrxn)
                           )
    
    A = scipy.sparse.vstack((S,L)).toarray()
    b = np.concatenate((np.zeros(nmetab), np.zeros(np.count_nonzero(ynt))))
    
    solver = pywraplp.Solver('fba',
                   pywraplp.Solver.CLP_LINEAR_PROGRAMMING)
    variables = [solver.NumVar(low,up,'x_%d'%ind) for ind,low,up in zip(range(nrxn),model.lb,model.ub)]
    ind_c = np.nonzero(model.f)[0][0]
    constraints = []
    for k in range(A.shape[0]):
        constr = solver.Constraint(b[k],b[k])
        r = A[k,:]
        for index in r.nonzero()[0]:
            constr.SetCoefficient(variables[index],A[k,index])
    constraints.append(constr)
    objective = solver.Objective()
    objective.SetCoefficient(variables[ind_c],1)
    if ptype[0] == 'max':
        objective.SetMaximization()
    else:
        objective.SetMinimization()
    
    # Solve first time
    solver.Solve()
    
    vbiomass = variables[ind_c].solution_value()
    constr_bio = solver.Constraint(vbiomass,vbiomass)
    constr_bio.SetCoefficient(variables[ind_c],1)
    constraints.append(constr)
    ind_f = np.nonzero(model.g)[0]
    objective = solver.Objective()
    for ind in ind_f:
        objective.SetCoefficient(variables[ind],model.g[ind])
        
    if ptype[1] == 'max':
        objective.SetMaximization()
    else:
        objective.SetMinimization()
    #Solve second time
    status = solver.Solve()
    if status != solver.OPTIMAL:
        if status != solver.FEASIBLE:
            print('Infeasible')
            return (-np.inf,np.inf)
            #raise Exception('Unfeasible solution, solver status: %d' %status)
        else:
            print('Suboptimal solution found')
    fmax = 0

    for ind in ind_f:
        fmax += (variables[ind].solution_value() * model.g[ind])
    
    if fluxes:
        return (vbiomass, fmax, np.array([var.solution_value() for var in variables]).copy())
    return (vbiomass, fmax)
    
def read_cobra_model(model):
    fbamodel = fbaModel()
    fbamodel.reactions = [r.id for r in model.reactions]
    fbamodel.nrnx = len(fbamodel.reactions)
    fbamodel.reaction_names = [r.name for r in model.reactions]
    fbamodel.lb = np.array([r.bounds[0] for r in model.reactions],dtype='double')
    fbamodel.ub = np.array([r.bounds[1] for r in model.reactions],dtype='double')
    fbamodel.p = [r.notes.get('EC NUMBER','') for r in model.reactions]
    fbamodel.subsytem = [r.subsystem for r in model.reactions]
    fbamodel.f = np.zeros(fbamodel.nrnx)
    fbamodel.g = np.zeros(fbamodel.nrnx)
    fbamodel.S = cobra.util.create_stoichiometric_matrix(model)
    fbamodel.metabs = [r.id for r in model.metabolites]
    fbamodel.present = np.array([r.functional for r in model.reactions],dtype=float)
    fbamodel.nmetabs = len(fbamodel.metabs)
    fbamodel.rules = [r.gene_reaction_rule for r in model.reactions]
    fbamodel.model = model
    return fbamodel

def load_mat(filename, model_name ='fbamodel'):
    def safe_list_get (l, idx, default):
        try:
            return l[idx]
        except IndexError:
            return default
    model = scipy.io.loadmat(filename)[model_name][0][0]
    reactions = [reaction[0][0] for reaction in model[0]]
    fbamodel = fbaModel()
    fbamodel.reactions = reactions
    fbamodel.nrnx = model[1][0][0]
    fbamodel.reaction_names = [reaction[0][0] for reaction in model[2]]
    fbamodel.lb = np.array([bound[0] for bound in model[3]],dtype='double')
    fbamodel.ub = np.array([bound[0] for bound in model[4]],dtype='double')
    fbamodel.pclass = [safe_list_get(pclass[0],0,'') for pclass in model[5]]
    fbamodel.subsistem = [pclass[0][0] for pclass in model[6]]    
    fbamodel.f = np.array([bound[0] for bound in model[7]],dtype='double')
    fbamodel.g = np.array([bound[0] for bound in model[8]],dtype='double')
    fbamodel.S = model[9]
    fbamodel.G = model[10]
    fbamodel.metabs =  [metab[0] for metab in model[11][0,:]]
    fbamodel.pts = [pts[0] for pts in model[12][0,:]]
    fbamodel.ko = np.array([k0[0] for k0 in model[13]],dtype='float')
    fbamodel.reverse = np.array([rev for rev in model[14][0,:]])
    fbamodel.fluxes =  [flux[0] for flux in model[15][0,:]]
    fbamodel.ind_fluxes =  np.array([flux for flux in model[16][0,:]])
    fbamodel.nmetabs = model[17][0][0]
    fbamodel.nbin = model[18][0][0]
    fbamodel.recmat = model[19]
    fbamodel.subclusters = [clust[0][0][0][0] for clust in model[20]]
    fbamodel.present = np.array([pres[0] for pres in model[21]])
    fbamodel.SS_reaction = model[22]
    fbamodel.SS_genes = model[23]
    fbamodel.nSS = model[24][0][0]
    fbamodel.SS_metabs = model[25]
    return fbamodel    

def load_mat_atp(filename, model_name ='fbamodel'):
    def safe_list_get (l, idx, default):
        try:
            return l[idx]
        except IndexError:
            return default
    model = scipy.io.loadmat(filename)[model_name][0][0]
    reactions = [reaction[0][0] for reaction in model[0]]
    fbamodel = fbaModel()
    fbamodel.reactions = reactions
    fbamodel.nrnx = model[1][0][0]
    fbamodel.reaction_names = [reaction[0][0] for reaction in model[2]]
    fbamodel.lb = np.array([bound[0] for bound in model[4]],dtype='double')
    fbamodel.ub = np.array([bound[0] for bound in model[5]],dtype='double')
    fbamodel.pclass = [safe_list_get(pclass[0],0,'') for pclass in model[6]]
    fbamodel.subsistem = [pclass[0][0] for pclass in model[7]]    
    fbamodel.f = np.array([bound[0] for bound in model[8]],dtype='double')
    fbamodel.g = np.array([bound[0] for bound in model[9]],dtype='double')
    fbamodel.S = model[10]
    fbamodel.G = model[11]
    fbamodel.metabs =  [metab[0] for metab in model[12][0,:]]
    fbamodel.pts = [pts[0] for pts in model[13][0,:]]
    fbamodel.ko = np.array([k0[0] for k0 in model[14]],dtype='float')
    fbamodel.reverse = np.array([rev for rev in model[16][0,:]])
    fbamodel.fluxes =  [flux[0] for flux in model[17][0,:]]
    fbamodel.ind_fluxes =  np.array([flux for flux in model[18][0,:]])
    fbamodel.nmetabs = model[19][0][0]
    fbamodel.nbin = model[20][0][0]
    fbamodel.recmat = model[21]
    #fbamodel.subclusters = [clust[0][0][0][0] for clust in model[20]]
    fbamodel.present = np.array([pres[0] for pres in model[3]])
    #fbamodel.SS_reaction = model[22]
    #fbamodel.SS_genes = model[23]
    #fbamodel.nSS = model[24][0][0]
    #fbamodel.SS_metabs = model[25]
    return fbamodel    


def load_c_difficile(filename, model_name ='fbamodel'):
    def safe_list_get (l, idx, default):
        try:
            return l[idx]
        except IndexError:
            return default
    model = scipy.io.loadmat(filename)[model_name][0][0]
    
    fbamodel = fbaModel()
    fbamodel.metabs = [metab[0][0] for metab in model[0]]
    fbamodel.metab_names = [metab[0][0] for metab in model[1]]
    fbamodel.reactions = [reaction[0][0] for reaction in model[3]]
    fbamodel.reaction_names = [safe_list_get(pclass[0],0,'') for pclass in model[4]]
    fbamodel.subsistem = [safe_list_get(pclass[0],0,'') for pclass in model[5]]
    fbamodel.lb = np.array([bound[0] for bound in model[6]],dtype='double')
    fbamodel.ub = np.array([bound[0] for bound in model[7]],dtype='double')
    fbamodel.reverse = np.array([rev[0] for rev in model[8]])
    fbamodel.c = np.array([bound[0] for bound in model[9]],dtype='double')
    fbamodel.b = np.array([bound[0] for bound in model[10]],dtype='double')
    fbamodel.S = model[11]
    fbamodel.rxnGeneMat = model[12]
    fbamodel.rules = [safe_list_get(pclass[0],0,'') for pclass in model[13]]
    fbamodel.gen_rules = [safe_list_get(pclass[0],0,'').replace("'",'') for pclass in model[14]]
    fbamodel.genes = [safe_list_get(pclass[0],0,'').replace("'",'') for pclass in model[15]]
    # blabla
    fbamodel.SS_reaction = model[30]
    fbamodel.nrnx = model[31][0][0]
    fbamodel.vmin = np.array([bound[0] for bound in model[32]],dtype='double')
    fbamodel.vmax = np.array([bound[0] for bound in model[33]],dtype='double')
    fbamodel.nmetabs = model[34][0][0]
    fbamodel.f = np.array([bound[0] for bound in model[35]],dtype='double')
    fbamodel.g = np.array([bound[0] for bound in model[36]],dtype='double')
    fbamodel.subclusters = [clust[0][0]for clust in model[37]]
    fbamodel.G = model[38]
    fbamodel.SS_genes = model[39]
    fbamodel.present = np.ones((fbamodel.nrnx,1))

    return fbamodel    

class fbaModel:
    
    def __init__(self):
        pass
        
if __name__ == "__main__":
    import time
    #model =cobra.io.read_sbml_model('msb201165-sup-0003.xml')
    model = load_mat('iJO1366_Ecoli_ac.mat')
    #model = load_c_difficile('icdf834.mat')
    t1 = time.time()
    print(solve_or_bylayer(model,solver='ortools'))
    print("Time eval :", time.time()-t1)    
